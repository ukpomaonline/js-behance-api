import C from '../types/constants';

export default (state = {user: ''}, action) => {
    switch(action.type) {
        case C.UPDATE_SEARCH:
            return {...state, user: action.payload.user};

        default:
            return state;
    }
}