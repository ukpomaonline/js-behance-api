import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from './components/Header';
import Home from './components/Home';
import './scss/main.scss';

class App extends Component {

    render () {
        return (
            <div>
                <Header />
                <Home />
            </div>
        )
    }
}

export default connect(state => state)(App);