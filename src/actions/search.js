import C from '../types/constants';

export default function updateSearch(user) {
    return dispatch => {
        dispatch({
            type: C.UPDATE_SEARCH,
            payload: {
                user
            }
        });

        return Promise.resolve();
    }
};