import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {

    render() {
        return (
            <div className='header'>
                <Link to='/' className='nav-item logo'>Behance Portfolios</Link>
                <Link to='/' className='nav-item menu-item'>Home</Link>
            </div>
        )
    }
}

export default Header;