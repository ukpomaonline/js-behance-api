import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class UserView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        }
    }

    openProfilePage() {
        this.setState({redirect: true});
    }

    render() {

        const { user, api_root, client_id } = this.props;

        if (this.state.redirect) {
            return <Redirect
                        to={{
                            pathname: user.username,
                            state: {
                                user: user,
                                api_root: api_root,
                                client_id: client_id
                            }
                        }} />;
        }

        return(
            <li onClick={this.openProfilePage.bind(this)}>
                <div className='icon'><img className='image' src={user.images['115']} /></div>
                <div className='details'>
                    <p>{user.first_name} {user.last_name}</p>
                    <p>{user.occupation}</p>
                    <div className='stats'>
                        <span><i className="far fa-thumbs-up"></i>{user.stats.appreciations}</span>
                        <span><i className="far fa-eye"></i>{user.stats.views}</span>
                        <span><i className="far fa-comment"></i>{user.stats.comments}</span>
                    </div>
                </div>
            </li>
        )
    }
}

export default UserView;