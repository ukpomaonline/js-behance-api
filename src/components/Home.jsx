import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import '../scss/home.scss';
import updateSearch from '../actions/search';
import UserView from '../components/UserView';

export class Home extends Component {

    constructor(props) {
        super(props);

        this.API_ROOT = 'https://cors.io/?https://api.behance.net/v2';
        this.CLIENT_ID = 'zqj9TiAFGMamf5l9dN26HC9rGN5UROzP';

        this.state = {
            users: [],
            suggestions: [],
            isLoading: false
        }
    }

    componentDidMount() {

        // reset the search value in the store
        this.updateSearch('');

        axios.get(`${this.API_ROOT}/creativestofollow/?client_id=${this.CLIENT_ID}`)
            .then(response => {
                this.setState({
                    suggestions: response.data.creatives_to_follow.slice(0, 3)
                });
            });
    }

    updateSearch(value) {
        this.props.dispatch(updateSearch(value))
            .then(() => {
                if(value) {
                    this.performSearch();
                }
            });
    }

    performSearch() {

        // extract the user searched for from the search reducer
        const { user } = this.props.searchReducer;

        if(user.length < 3) {
            this.setState({ users: [] });
            return;
        }

        this.setState({isLoading: true});

        axios.get(`${this.API_ROOT}/users/?q=${user}&client_id=${this.CLIENT_ID}`)
            .then(response => {
                this.setState({
                    users: response.data.users,
                    isLoading: false
                });
            });
    }

    render() {

        const { user } = this.props.searchReducer;

        return (
            <div id='home' className='homepage'>
                <div className='top'>
                    <div className="dim-overlay"></div>
                    <span id='home-title' className='title'>Discover the Best!</span>
                    <div className='image-overlay'>
                        <img src={require('../images/homepage-header.jpg')} alt='homepage header' />
                    </div>
                </div>
                <div className='container'>
                    <div className='search-box'>
                        <input placeholder='Search for someone...' type='text' autoComplete='off' onChange={(e) => this.updateSearch(e.target.value)} />
                    </div>
                    <div className='results'>
                        {this.state.isLoading &&
                            <div className='loading-container'>
                                <img className='loader' src={require('../images/loader-icon.gif')} alt='loading...' />
                            </div>
                        }
                        {!this.state.isLoading && this.state.users.length > 0 &&
                            <ul className='users'>
                                {this.state.users.map((user, index) => {
                                    return <UserView key={index} user={user} api_root={this.API_ROOT} client_id={this.CLIENT_ID} />
                                })}
                            </ul>
                        }
                        {!this.state.isLoading && user.length >= 3 && !this.state.users.length > 0 &&
                            <p>No users found</p>
                        }
                        {!this.state.isLoading && !this.state.users.length && this.state.suggestions.length > 0 &&
                           <div className='suggestions'>
                               <p className='title'>You might be interested in</p>
                               <ul className='users'>
                                   {this.state.suggestions.map((user, index) => {
                                       return <UserView key={index} user={user} api_root={this.API_ROOT} client_id={this.CLIENT_ID} />
                                   })}
                               </ul>
                           </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(state => state)(Home);