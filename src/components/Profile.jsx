import React, { Component } from 'react';
import Header from '../components/Header';
import axios from 'axios';
import ProfileView from './ProfileView';
import '../scss/profile.scss';

class Profile extends Component {

    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            first_name: '',
            last_name: '',
            occupation: '',
            location: '',
            stats: {},
            profile_pic: '',
            projects: [],
            work_experience: [],
            following: [],
            followers: [],
            tab_view: 'projects',
            tabs_class: {
                projects: 'active',
                work_experience: '',
                following: '',
                followers: ''
            }
        }
    }

    componentDidMount () {

        const { user } = this.props.location.state;

        this.setState({
            first_name: user.first_name,
            last_name: user.last_name,
            occupation: user.occupation,
            location: user.location,
            stats: user.stats,
            profile_pic: user.images['138']
        });

        this.retrieveData('projects')
            .then((data) => {
                this.setState({
                    projects: data
                })
            });
    }

    retrieveData(endpoint) {

        const { handle } = this.props.match.params;

        const { api_root, client_id } = this.props.location.state;

        this.setState({isLoading: true});

        return axios.get(`${api_root}/users/${handle}/${endpoint}?client_id=${client_id}`)
            .then(response => {
                this.setState({isLoading: false});
                return response.data[endpoint];
            });
    }

    setTabView(view) {

        this.setState({isLoading: false});

        this.setState({
            tab_view: view,
            tabs_class: {
                projects: '',
                work_experience: '',
                following: '',
                followers: ''
            },
        }, () => {
            this.setState({
                tabs_class: {
                    ...this.state.tabs_class,
                    [view]: 'active'
                }
            })
        });

        if(!this.state[view].length) {
            this.retrieveData(view)
                .then((data) => {
                    this.setState({
                        [view]: data
                    })
                });
        }
    }

    render() {
        return (
            <div className='profile'>
                <Header />

                <div className='container'>
                    <div className='details'>
                        <div className='pic'>
                            <img src={this.state.profile_pic} />
                        </div>
                        <div className='info'>
                            <p className='name'>{this.state.first_name} {this.state.last_name}</p>
                            <div className='about'>
                                <p><i className="fas fa-briefcase"></i> {this.state.occupation}</p>
                                <p><i className="fas fa-map-marker-alt"></i> {this.state.location}</p>
                            </div>
                            <p className='stats'>
                                <span><i className="far fa-thumbs-up"></i> {this.state.stats.appreciations}</span>
                                <span><i className="far fa-eye"></i> {this.state.stats.views}</span>
                                <span><i className="far fa-comment"></i> {this.state.stats.comments}</span>
                            </p>
                            <p className='stats friends'>
                                <span><b>{this.state.stats.following}</b> Following</span>
                                <span><b>{this.state.stats.followers}</b> Followers</span>
                            </p>
                        </div>
                    </div>

                    <div className='navigation'>
                        <span className={this.state.tabs_class.projects} onClick={this.setTabView.bind(this, 'projects')}>Projects</span>
                        <span className={this.state.tabs_class.work_experience} onClick={this.setTabView.bind(this, 'work_experience')}>Work Experience</span>
                        <span className={this.state.tabs_class.following} onClick={this.setTabView.bind(this, 'following')}>Following</span>
                        <span className={this.state.tabs_class.followers} onClick={this.setTabView.bind(this, 'followers')}>Followers</span>
                    </div>

                    <div className='content'>

                        {this.state.isLoading &&
                            <div className='loading-container'>
                                <img className='loader' src={require('../images/loader-icon.gif')} alt='loading...' />
                            </div>
                        }

                        <ul className='items'>
                            {this.state.tab_view === 'projects' && this.state.projects.map((project, index) => {
                                return (
                                    <li className='cell projects' key={index}>
                                        <a href={project.url} target='_blank'>
                                            <ProfileView  image_url={project.covers['230']} name={project.name} stats={project.stats} />
                                        </a>
                                    </li>
                                )
                            })}

                            {this.state.tab_view === 'work_experience' && this.state.work_experience.map((work, index) => {
                                return (
                                    <li className='work' key={index}>
                                        <p className='organization'>{work.organization}, <span className='location'>{work.location}</span></p>
                                        <p>{work.position}</p>
                                        <p>Start Date: {work.start_date}</p>
                                        {work.end_date &&
                                            <p>End Date: {work.end_date}</p>
                                        }
                                    </li>
                                )
                            })}

                            {this.state.tab_view === 'following' && this.state.following.map((following, index) => {
                                return (
                                    <li className='cell following' key={index}>
                                        <ProfileView  image_url={following.images['230']} name={`${following.first_name} ${following.last_name}`} stats={following.stats} />
                                    </li>
                                )
                            })}

                            {this.state.tab_view === 'followers' && this.state.followers.map((followers, index) => {
                                return (
                                    <li className='cell following' key={index}>
                                        <ProfileView  image_url={followers.images['230']} name={`${followers.first_name} ${followers.last_name}`} stats={followers.stats} />
                                    </li>
                                )
                            })}

                            <li className='cell invisible' key='a'></li>
                            <li className='cell invisible' key='b'></li>
                            <li className='cell invisible' key='c'></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile;