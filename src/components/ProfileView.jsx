import React, { Component } from 'react';

class ProfileView extends Component {

    render() {

        const { image_url, name, stats } = this.props;

        return(
            <div className='block'>
                <img src={image_url} />
                <div className='project-info'>
                    <p className='title'>{name}</p>
                    <div className='stats'>
                        <span><i className="far fa-thumbs-up"></i> {stats.appreciations}</span>
                        <span><i className="far fa-eye"></i> {stats.views}</span>
                        <span><i className="far fa-comment"></i> {stats.comments}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProfileView;