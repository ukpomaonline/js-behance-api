import searchReducer from '../src/reducers/search';
import C from '../src/types/constants';

describe('search reducer', () => {
    it('should return the initial state', () => {
        expect(searchReducer(undefined, {})).toEqual({
            user: ''
        });
    });

    it('should handle UPDATE_SEARCH', () => {
        expect(
            searchReducer({}, {
                type: C.UPDATE_SEARCH,
                payload: {
                    user: 'sam'
                }
            })).toEqual({
                user: 'sam'
            });

        expect(
            searchReducer({user: 'tr'}, {
                type: C.UPDATE_SEARCH,
                payload: {
                    user: 'tra'
                }
            })).toEqual({
            user: 'tra'
        });
    });
});