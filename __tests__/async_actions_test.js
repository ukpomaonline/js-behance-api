import updateSearch from '../src/actions/search';
import C from '../src/types/constants';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
    it('should create an action to update the user searched for', () => {

        const user = 'Mat';

        const expectedAction = [{
            type: C.UPDATE_SEARCH,
            payload: {
                user
            }
        }];

        const store = mockStore({user: ''});

        return store.dispatch(updateSearch(user)).then((actionAction) => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedAction)
        });
    });
});