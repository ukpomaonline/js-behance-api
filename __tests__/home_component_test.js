import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Home } from '../src/components/Home';
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

Enzyme.configure({ adapter: new Adapter() });

function setup() {

    const store = mockStore({user: ''});

    const props = {
        searchReducer: {user: ''},
        dispatch: store.dispatch
    };

    const enzymeWrapper = shallow(<Home {...props} />);

    return {
        props,
        enzymeWrapper
    }
}

describe('components', () => {
    describe('Home', () => {
        it('should render self and subcomponents', () => {
            const {enzymeWrapper} = setup();

            expect(enzymeWrapper.find('div#home').hasClass('homepage')).toBe(true);

            expect(enzymeWrapper.find('span#home-title').text()).toBe('Discover the Best!');
        });
    });
});