import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import searchReducer from './src/reducers/search';

const reducer = combineReducers({
    searchReducer
});

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export default store;