# Dealer Inspire Front End Code Challenge

This is a beautifully designed and fully responsive user search application utilizing the Behance API, built in ReactJS, Redux, React Router and Axios (used to fetch the data).

When the site starts up, the user is prompted with a search input field and after typing 3 or more characters, a real-time search is performed upon each character entered/removed. Once an option is selected from the list of people presented, the user is then taken to the selected profile page showing additional details such as the persons projects, work experience, followers list, following list, etc.

## Setup
- Clone this repo by running `git clone https://ukpomaonline@bitbucket.org/ukpomaonline/js-behance-api.git`
- Change into the `js-behance-api` directory
- Run `npm install`
- Then start the project by running `npm run dev`
- Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

## Additional Scripts
In addition to the scripts above, you can run:

### `npm test`
This will run all available tests in the project

### `npm run test:watch`
This will run all available tests in the project, in an interactive mode (i.e. any additional changes to the test scripts will immediately get run)

## Screenshots
- [Desktop Homepage](https://bitbucket.org/ukpomaonline/js-behance-api/raw/e7fe2bee9c511d71eb0a6d8b1855aeb106e4afa3/src/images/home-desktop.png)

- [Desktop Profile Page](https://bitbucket.org/ukpomaonline/js-behance-api/raw/e7fe2bee9c511d71eb0a6d8b1855aeb106e4afa3/src/images/profile-desktop.png)

- [Mobile Homepage](https://bitbucket.org/ukpomaonline/js-behance-api/raw/e7fe2bee9c511d71eb0a6d8b1855aeb106e4afa3/src/images/home-mobile.png)

- [Mobile Profile Page](https://bitbucket.org/ukpomaonline/js-behance-api/raw/e7fe2bee9c511d71eb0a6d8b1855aeb106e4afa3/src/images/profile-mobile.png)